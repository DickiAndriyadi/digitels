package constant

const ERROR_CUSTOM_REQUIRED = "The %s field is required."
const ERROR_MODEL_NOT_FOUND = "Model does not exists."
