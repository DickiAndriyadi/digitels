package repository

import (
	"digitels/api/model"

	"github.com/jinzhu/gorm"
)

func (repository) UpdateToken(db *gorm.DB, tokenMap, whereMap map[string]interface{}) error {
	var (
		user model.User
	)

	err := db.Where(whereMap).Model(&user).Updates(tokenMap).Error
	if err != nil {
		return err
	}

	return nil

}
