package repository

import (
	"digitels/api/model"

	"github.com/jinzhu/gorm"
)

func (repository) Register(db *gorm.DB, store *model.User) (*model.User, error) {

	var (
		user model.User
	)

	err := db.Table(user.TableName()).Create(&store).Last(&user).Error
	if err != nil {
		return nil, err
	}

	return &user, nil
}
