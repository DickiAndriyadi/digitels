package repository

import (
	"github.com/jinzhu/gorm"

	"digitels/api/model"
)

func (repository) Detail(db *gorm.DB, payload map[string]interface{}) (*model.User, error) {
	var user model.User

	q := db.Model(model.User{})

	err := q.Where(payload).Where("deleted_at is NULL").Find(&user).Error

	if err != nil {
		return nil, err
	}

	return &user, nil
}
