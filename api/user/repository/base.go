package repository

import "digitels/api/user"

type repository struct{}

func NewRepository() user.Repository {
	return &repository{}
}
