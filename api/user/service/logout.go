package service

import (
	"digitels/api/model/response"
	"net/http"
)

func (s *service) Logout(token string) *response.ServiceError {
	tokenMap := map[string]interface{}{
		"token": nil,
	}

	whereMap := map[string]interface{}{
		"token": token,
	}

	err := s.userRepository.UpdateToken(s.dbManager.GetDB(), tokenMap, whereMap)

	if err != nil {
		return &response.ServiceError{
			Code: http.StatusNotFound,
			Err:  err,
		}
	}

	return nil
}
