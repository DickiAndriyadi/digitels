package service

import (
	"digitels/api/user"
	"digitels/config/db"
)

type service struct {
	dbManager      db.DatabaseManager
	userRepository user.Repository
}

func NewService(
	dbManager db.DatabaseManager,
	userRepository user.Repository,
) user.Service {
	return &service{
		dbManager,
		userRepository,
	}
}
