package service

import (
	"digitels/api/model"
	"digitels/api/model/payload"
	"digitels/api/model/response"
	"digitels/helper/utils"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/jinzhu/gorm"
)

func (s *service) Register(p *payload.RegisterUser) (*model.User, *response.ServiceError) {

	registerMap := map[string]interface{}{
		"username": p.Username,
	}

	_, err := s.userRepository.Detail(s.dbManager.GetDB(), registerMap)
	if err == nil {
		if !gorm.IsRecordNotFoundError(err) {
			return nil, &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  errors.New(fmt.Sprintf("User with username %s is already used", p.Username)),
			}
		}
	}

	var (
		user model.User
	)

	loc, err := time.LoadLocation("Asia/Jakarta")

	user.Username = p.Username
	user.Password = utils.CryptPassword(p.Password)
	user.Level = "user"
	user.CreatedAt = time.Now().In(loc).Format("2006-01-02 15:04:05")
	user.UpdatedAt = time.Now().In(loc).Format("2006-01-02 15:04:05")

	data, errCreate := s.userRepository.Register(s.dbManager.GetDB(), &user)
	if errCreate != nil {
		return nil, &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  errCreate,
		}
	}

	return data, nil
}
