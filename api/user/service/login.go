package service

import (
	"digitels/api/model"
	"digitels/api/model/response"
	"digitels/helper/utils"
	"errors"
	"net/http"
	"strings"
	"time"

	jwt "github.com/golang-jwt/jwt/v4"
	"github.com/jinzhu/gorm"
)

func (s *service) Login(username, password string) (*map[string]interface{}, *response.ServiceError) {

	loginMap := map[string]interface{}{
		"username": username,
	}

	user, err := s.userRepository.Detail(s.dbManager.GetDB(), loginMap)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  err,
			}
		}
	}

	isTrue := utils.CompareCrypt(password, user.Password)
	if !isTrue {
		return nil, &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  errors.New("Wrong password!"),
		}
	}

	claims := &model.JwtCustomClaims{
		user.Username,
		user.ID,
		user.Level,
		jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(24 * time.Hour)),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
			Issuer:    "digitels",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	t, err := token.SignedString([]byte("TestDigitels"))
	nowUnix := time.Now().Add(time.Hour * 24).Unix()
	tm := time.Unix(nowUnix, 0)
	exp := strings.Replace(tm.String(), "+0700 +07", "", -1)

	res := map[string]interface{}{
		"expired": exp,
		"token":   t,
	}

	tokenMap := map[string]interface{}{
		"token": t,
	}

	whereMap := map[string]interface{}{
		"id": user.ID,
	}

	err = s.userRepository.UpdateToken(s.dbManager.GetDB(), tokenMap, whereMap)
	if err != nil {
		return nil, &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  err,
		}
	}

	return &res, nil
}
