package controller

import (
	"digitels/api/model/payload"
	"digitels/api/model/response"
	"net/http"

	"github.com/labstack/echo"
)

func (ctrl *UserController) Register(c echo.Context) (err error) {

	var (
		result response.ItemsResponse
	)

	payload := new(payload.RegisterUser)

	if err := c.Bind(payload); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	if err := payload.Validate(); err != nil {
		return echo.NewHTTPError(
			http.StatusBadRequest,
			err,
		)
	}

	data, serviceErr := ctrl.service.Register(payload)
	if serviceErr != nil {
		return serviceErr.HttpError()
	}

	result.Data.Items = data

	return c.JSON(http.StatusCreated, result)
}
