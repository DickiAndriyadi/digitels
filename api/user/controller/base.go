package controller

import "digitels/api/user"

type UserController struct {
	service user.Service
}

func NewController(s user.Service) *UserController {
	return &UserController{service: s}
}
