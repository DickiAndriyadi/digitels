package controller

import (
	"digitels/api/model/response"
	"net/http"
	"strings"

	"github.com/labstack/echo"
)

func (ctrl *UserController) Logout(c echo.Context) (err error) {
	var (
		result response.DataResponse
	)

	reqToken := c.Request().Header.Get("Authorization")
	splitToken := strings.Split(reqToken, "Bearer ")
	reqToken = splitToken[1]

	serviceErr := ctrl.service.Logout(reqToken)
	if serviceErr != nil {
		return serviceErr.HttpError()
	}

	result.Data = "Success logout!"

	return c.JSON(http.StatusOK, result)
}
