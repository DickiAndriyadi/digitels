package controller

import (
	"digitels/api/model/response"
	"errors"
	"net/http"

	"github.com/labstack/echo"
)

func (ctrl *UserController) Login(c echo.Context) (err error) {

	var (
		result response.DataResponse
	)

	username, password, ok := c.Request().BasicAuth()
	defer c.Request().Body.Close()

	if !ok {
		return echo.NewHTTPError(
			http.StatusBadRequest,
			errors.New("Bad request!"),
		)
	}

	data, serviceErr := ctrl.service.Login(username, password)
	if serviceErr != nil {
		return serviceErr.HttpError()
	}

	result.Data = data

	return c.JSON(http.StatusOK, result)
}
