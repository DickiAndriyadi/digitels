package user

import (
	"digitels/api/model"
	"digitels/api/model/payload"
	"digitels/api/model/response"
)

type Service interface {
	Login(username, password string) (*map[string]interface{}, *response.ServiceError)
	Logout(token string) *response.ServiceError
	Register(p *payload.RegisterUser) (*model.User, *response.ServiceError)
}
