package user

import (
	"digitels/api/model"

	"github.com/jinzhu/gorm"
)

type Repository interface {
	Register(db *gorm.DB, store *model.User) (*model.User, error)
	Detail(db *gorm.DB, payload map[string]interface{}) (*model.User, error)
	UpdateToken(db *gorm.DB, tokenMap, whereMap map[string]interface{}) error
}
