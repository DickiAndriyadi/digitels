package repository

import (
	"digitels/api/model"

	"github.com/jinzhu/gorm"
)

func (repository) Create(db *gorm.DB, store *model.Employee) (*model.Employee, error) {

	var (
		employee model.Employee
	)

	err := db.Preload("User").Table(employee.TableName()).Create(&store).Last(&employee).Error
	if err != nil {
		return nil, err
	}

	return &employee, nil
}
