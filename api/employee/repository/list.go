package repository

import (
	"digitels/api/model"

	"github.com/jinzhu/gorm"
)

func (repository) List(db *gorm.DB) (*model.Employees, error) {

	employee := new(model.Employee)
	var listEmployee model.Employees

	q := db.Table(employee.TableName())

	err := q.Preload("User").Where("deleted_at is NULL").Find(&listEmployee).Error

	if err != nil {
		return nil, err
	}

	return &listEmployee, nil
}
