package repository

import (
	"digitels/api/model"
	"digitels/api/model/payload"

	"github.com/jinzhu/gorm"
)

func (repository) Update(db *gorm.DB, store *payload.UpdateEmployee, id string) (*model.Employee, error) {

	var employee model.Employee

	err := db.Preload("User").Where("id = ?", id).Model(&employee).UpdateColumns(store).Find(&employee).Error
	if err != nil {
		return nil, err
	}

	return &employee, nil
}
