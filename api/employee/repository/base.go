package repository

import "digitels/api/employee"

type repository struct{}

func NewRepository() employee.Repository {
	return &repository{}
}
