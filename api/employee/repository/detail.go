package repository

import (
	"digitels/api/model"

	"github.com/jinzhu/gorm"
)

func (repository) Detail(db *gorm.DB, payload map[string]interface{}) (*model.Employee, error) {

	employee := new(model.Employee)

	q := db.Model(employee)

	err := q.Preload("User").Where(payload).Find(&employee).Error

	if err != nil {
		return nil, err
	}

	return employee, nil
}
