package repository

import (
	"digitels/api/model"

	"github.com/jinzhu/gorm"
)

func (repository) Delete(db *gorm.DB, deleteMap map[string]interface{}) error {

	var (
		employee model.Employee
	)

	err := db.Where(deleteMap).Delete(&employee).Error
	if err != nil {
		return err
	}

	return nil
}
