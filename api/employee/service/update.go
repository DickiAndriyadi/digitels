package service

import (
	"digitels/api/model"
	"digitels/api/model/payload"
	"digitels/api/model/response"
	"errors"
	"net/http"
	"time"

	"github.com/jinzhu/gorm"
)

func (s *service) Update(p *payload.UpdateEmployee, id, tokenString string) (*model.Employee, *response.ServiceError) {

	detailMap := map[string]interface{}{
		"id": id,
	}

	employee, err := s.employeeRepository.Detail(s.dbManager.GetDB(), detailMap)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  errors.New("data not found!"),
			}
		}

		return nil, &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  err,
		}
	}

	createMap := map[string]interface{}{
		"token": tokenString,
	}

	user, err := s.userRepository.Detail(s.dbManager.GetDB(), createMap)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  errors.New("User is not found!"),
			}
		}

		return nil, &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  err,
		}

	}

	if user.Level == "user" {
		if user.ID != employee.UserID {
			return nil, &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  errors.New("cannot create other than own user id"),
			}
		}

	}

	loc, err := time.LoadLocation("Asia/Jakarta")

	p.UpdatedAt = time.Now().In(loc).Format("2006-01-02 15:04:05")

	data, errUpdate := s.employeeRepository.Update(s.dbManager.GetDB(), p, id)
	if errUpdate != nil {

		return nil, &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  errUpdate,
		}
	}

	return data, nil
}
