package service

import (
	"digitels/api/model/response"
	"errors"
	"net/http"

	"github.com/jinzhu/gorm"
)

func (s *service) Delete(id, tokenString string) *response.ServiceError {

	detailMap := map[string]interface{}{
		"id": id,
	}

	employee, err := s.employeeRepository.Detail(s.dbManager.GetDB(), detailMap)

	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  errors.New("data not found!"),
			}
		}
		return &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  err,
		}

	}

	userMap := map[string]interface{}{
		"token": tokenString,
	}

	user, err := s.userRepository.Detail(s.dbManager.GetDB(), userMap)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  errors.New("User is not found!"),
			}
		}

		return &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  err,
		}

	}

	if user.Level == "user" {
		if user.ID != employee.UserID {
			return &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  errors.New("cannot delete other than your own data"),
			}
		}

	}

	deleteMap := map[string]interface{}{
		"id": id,
	}

	errDelete := s.employeeRepository.Delete(s.dbManager.GetDB(), deleteMap)
	if errDelete != nil {
		return &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  errDelete,
		}
	}

	return nil
}
