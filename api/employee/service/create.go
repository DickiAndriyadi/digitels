package service

import (
	"digitels/api/model"
	"digitels/api/model/payload"
	"digitels/api/model/response"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/jinzhu/gorm"
)

func (s *service) Create(p *payload.CreateEmployee, token string) (*model.Employee, *response.ServiceError) {

	createMap := map[string]interface{}{
		"token": token,
	}

	user, err := s.userRepository.Detail(s.dbManager.GetDB(), createMap)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  errors.New("User is not found!"),
			}
		}

		return nil, &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  err,
		}

	}

	if user.Level == "user" {
		if user.ID != p.UserID {
			return nil, &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  errors.New("cannot create other than own user id"),
			}
		}

	} else if user.Level == "admin" {

		adminMap := map[string]interface{}{
			"id":    p.UserID,
			"level": "user",
		}

		_, err := s.userRepository.Detail(s.dbManager.GetDB(), adminMap)
		if err != nil {
			if gorm.IsRecordNotFoundError(err) {
				return nil, &response.ServiceError{
					Code: http.StatusBadRequest,
					Err:  errors.New(fmt.Sprintf("User with User ID %d is not found!", p.UserID)),
				}
			}
		}
	}

	var (
		employee model.Employee
	)

	loc, err := time.LoadLocation("Asia/Jakarta")

	employee.UserID = p.UserID
	employee.Name = p.Name
	employee.Email = p.Email
	employee.Division = p.Division
	employee.CreatedAt = time.Now().In(loc).Format("2006-01-02 15:04:05")
	employee.UpdatedAt = time.Now().In(loc).Format("2006-01-02 15:04:05")

	data, errCreate := s.employeeRepository.Create(s.dbManager.GetDB(), &employee)
	if errCreate != nil {
		return nil, &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  errCreate,
		}
	}

	return data, nil
}
