package service

import (
	"digitels/api/employee"
	"digitels/api/user"
	"digitels/config/db"
)

type service struct {
	dbManager          db.DatabaseManager
	employeeRepository employee.Repository
	userRepository     user.Repository
}

func NewService(
	dbManager db.DatabaseManager,
	employeeRepository employee.Repository,
	userRepository user.Repository,
) employee.Service {
	return &service{
		dbManager,
		employeeRepository,
		userRepository,
	}
}
