package employee

import (
	"digitels/api/model"
	"digitels/api/model/payload"

	"github.com/jinzhu/gorm"
)

type Repository interface {
	Create(db *gorm.DB, store *model.Employee) (*model.Employee, error)
	List(db *gorm.DB) (*model.Employees, error)
	Detail(db *gorm.DB, payload map[string]interface{}) (*model.Employee, error)
	Delete(db *gorm.DB, deleteMap map[string]interface{}) error
	Update(db *gorm.DB, store *payload.UpdateEmployee, id string) (*model.Employee, error)
}
