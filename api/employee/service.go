package employee

import (
	"digitels/api/model"
	"digitels/api/model/payload"
	"digitels/api/model/response"
)

type Service interface {
	Create(p *payload.CreateEmployee, token string) (*model.Employee, *response.ServiceError)
	List() (*model.Employees, *response.ServiceError)
	Detail(id string) (*model.Employee, *response.ServiceError)
	Delete(id, token string) *response.ServiceError
	Update(p *payload.UpdateEmployee, id, tokenString string) (*model.Employee, *response.ServiceError)
}
