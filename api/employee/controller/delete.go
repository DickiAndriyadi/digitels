package controller

import (
	"digitels/api/model/response"
	"net/http"
	"strings"

	"github.com/labstack/echo"
)

func (ctrl *EmployeeController) Delete(c echo.Context) error {

	var (
		result response.DataResponse
	)

	id := c.Param("id")

	authorizationHeader := c.Request().Header.Get("Authorization")
	tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

	errDelete := ctrl.service.Delete(id, tokenString)
	if errDelete != nil {
		return errDelete.HttpError()
	}

	result.Data = "Success delete data!"

	return c.JSON(http.StatusOK, result)
}
