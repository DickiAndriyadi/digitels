package controller

import (
	"digitels/api/model/payload"
	"digitels/api/model/response"
	"net/http"
	"strings"

	"github.com/labstack/echo"
)

func (ctrl *EmployeeController) Create(c echo.Context) (err error) {

	var (
		result response.ItemsResponse
	)

	payload := new(payload.CreateEmployee)

	if err := c.Bind(payload); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	if err := payload.Validate(); err != nil {
		return echo.NewHTTPError(
			http.StatusBadRequest,
			err.Error(),
		)
	}

	authorizationHeader := c.Request().Header.Get("Authorization")
	tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

	data, serviceErr := ctrl.service.Create(payload, tokenString)
	if serviceErr != nil {
		return serviceErr.HttpError()
	}

	result.Data.Items = data

	return c.JSON(http.StatusCreated, result)
}
