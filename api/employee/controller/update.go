package controller

import (
	"digitels/api/model/payload"
	"digitels/api/model/response"
	"net/http"
	"strings"

	"github.com/labstack/echo"
)

func (ctrl *EmployeeController) Update(c echo.Context) error {

	var (
		result response.ItemsResponse
	)

	id := c.Param("id")
	authorizationHeader := c.Request().Header.Get("Authorization")
	tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

	payload := new(payload.UpdateEmployee)

	if err := c.Bind(payload); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	data, err := ctrl.service.Update(payload, id, tokenString)
	if err != nil {
		return err.HttpError()
	}

	result.Data.Items = data

	return c.JSON(http.StatusOK, result)
}
