package controller

import (
	"digitels/api/model/response"
	"net/http"

	"github.com/labstack/echo"
)

func (ctrl *EmployeeController) List(c echo.Context) (err error) {

	var (
		result response.ItemsResponse
	)

	data, serviceErr := ctrl.service.List()
	if serviceErr != nil {
		return serviceErr.HttpError()
	}

	result.Data.Items = data

	return c.JSON(http.StatusOK, result)
}
