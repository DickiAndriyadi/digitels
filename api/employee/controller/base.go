package controller

import "digitels/api/employee"

type EmployeeController struct {
	service employee.Service
}

func NewController(s employee.Service) *EmployeeController {
	return &EmployeeController{service: s}
}
