package repository

import (
	"digitels/api/model"

	"github.com/jinzhu/gorm"
)

func (repository) Detail(db *gorm.DB, payload map[string]interface{}) (*model.Attendance, error) {

	attendance := new(model.Attendance)

	q := db.Model(attendance)

	err := q.Where(payload).Find(&attendance).Error

	if err != nil {
		return nil, err
	}

	return attendance, nil
}
