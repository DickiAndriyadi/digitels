package repository

import (
	"digitels/api/model"

	"github.com/jinzhu/gorm"
)

func (repository) Delete(db *gorm.DB, deleteMap map[string]interface{}) error {

	var (
		attendance model.Attendance
	)

	err := db.Where(deleteMap).Delete(&attendance).Error
	if err != nil {
		return err
	}

	return nil
}
