package repository

import (
	"digitels/api/model"

	"github.com/jinzhu/gorm"
)

func (repository) ClockIn(db *gorm.DB, store *model.Attendance) (*model.Attendance, error) {

	var (
		attendance model.Attendance
	)

	err := db.Table(attendance.TableName()).Create(&store).Last(&attendance).Error
	if err != nil {
		return nil, err
	}

	return &attendance, nil
}
