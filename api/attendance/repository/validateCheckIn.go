package repository

import (
	"digitels/api/model"
	"time"

	"github.com/jinzhu/gorm"
)

func (repository) ValidateCheckIn(db *gorm.DB, id uint64) (int64, error) {

	var (
		attendance model.Attendance
		count      int64
	)

	loc, _ := time.LoadLocation("Asia/Jakarta")
	today := time.Now().In(loc).Format("2006-01-02")

	err := db.Table(attendance.TableName()).Where("DATE(SUBSTRING(clock_in, 1, 10)) = ? AND employee_id = ?", today, id).Count(&count).Error
	if err != nil {
		return 0, err
	}

	return count, nil
}
