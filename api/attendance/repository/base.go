package repository

import "digitels/api/attendance"

type repository struct{}

func NewRepository() attendance.Repository {
	return &repository{}
}
