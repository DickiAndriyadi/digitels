package repository

import (
	"digitels/api/model"

	"github.com/jinzhu/gorm"
)

func (repository) List(db *gorm.DB) (*model.Attendances, error) {

	attendance := new(model.Attendance)
	var listAttendance model.Attendances

	q := db.Table(attendance.TableName())

	err := q.Where("deleted_at is NULL").Order("clock_in").Find(&listAttendance).Error

	if err != nil {
		return nil, err
	}

	return &listAttendance, nil
}
