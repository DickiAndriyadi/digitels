package repository

import (
	"digitels/api/model"
	"digitels/api/model/payload"

	"github.com/jinzhu/gorm"
)

func (repository) ClockOut(db *gorm.DB, store *payload.ClockOut, id string) (*model.Attendance, error) {

	var attendance model.Attendance

	err := db.Where("id = ?", id).Model(&attendance).UpdateColumns(store).Find(&attendance).Error
	if err != nil {
		return nil, err
	}

	return &attendance, nil
}
