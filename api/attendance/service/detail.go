package service

import (
	"digitels/api/model"
	"digitels/api/model/response"
	"errors"
	"net/http"

	"github.com/jinzhu/gorm"
)

func (s *service) Detail(id string) (*model.Attendance, *response.ServiceError) {

	detailMap := map[string]interface{}{
		"id": id,
	}

	res, err := s.attendanceRepository.Detail(s.dbManager.GetDB(), detailMap)

	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  errors.New("data not found!"),
			}
		}

		return nil, &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  err,
		}
	}

	return res, nil
}
