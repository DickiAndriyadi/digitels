package service

import (
	"digitels/api/attendance"
	"digitels/api/employee"
	"digitels/api/user"
	"digitels/config/db"
)

type service struct {
	dbManager            db.DatabaseManager
	attendanceRepository attendance.Repository
	employeeRepository   employee.Repository
	userRepository       user.Repository
}

func NewService(
	dbManager db.DatabaseManager,
	attendanceRepository attendance.Repository,
	employeeRepository employee.Repository,
	userRepository user.Repository,

) attendance.Service {
	return &service{
		dbManager,
		attendanceRepository,
		employeeRepository,
		userRepository,
	}
}
