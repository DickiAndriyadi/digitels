package service

import (
	"digitels/api/model"
	"digitels/api/model/payload"
	"digitels/api/model/response"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/jinzhu/gorm"
)

func (s *service) ClockIn(p *payload.ClockIn, token string) (*model.Attendance, *response.ServiceError) {

	createMap := map[string]interface{}{
		"token": token,
	}

	user, err := s.userRepository.Detail(s.dbManager.GetDB(), createMap)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  errors.New("User is not found!"),
			}
		}

		return nil, &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  err,
		}

	}

	detailMap := map[string]interface{}{
		"id": p.EmployeeID,
	}

	employee, err := s.employeeRepository.Detail(s.dbManager.GetDB(), detailMap)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  errors.New(fmt.Sprintf("Employee with Employee ID %d is not found!", p.EmployeeID)),
			}
		}

		return nil, &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  err,
		}
	}

	countAttendance, err := s.attendanceRepository.ValidateCheckIn(s.dbManager.GetDB(), employee.ID)
	if err != nil {
		return nil, &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  err,
		}
	}

	if countAttendance > 0 {
		return nil, &response.ServiceError{
			Code: http.StatusNotAcceptable,
			Err:  errors.New("already attendance today"),
		}
	}

	if user.Level == "user" {
		if user.ID != employee.UserID {
			return nil, &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  errors.New("cannot clock in other employee"),
			}
		}
	}

	var (
		attendance model.Attendance
	)

	loc, err := time.LoadLocation("Asia/Jakarta")

	attendance.EmployeeID = employee.ID
	attendance.ClockIn = time.Now().In(loc).Format("2006-01-02 15:04:05")
	attendance.CreatedAt = time.Now().In(loc).Format("2006-01-02 15:04:05")
	attendance.UpdatedAt = time.Now().In(loc).Format("2006-01-02 15:04:05")

	data, errClockIn := s.attendanceRepository.ClockIn(s.dbManager.GetDB(), &attendance)
	if errClockIn != nil {
		return nil, &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  errClockIn,
		}
	}

	return data, nil
}
