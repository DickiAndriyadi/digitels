package service

import (
	"digitels/api/model"
	"digitels/api/model/response"
	"errors"
	"net/http"
)

func (s *service) List() (*model.Attendances, *response.ServiceError) {

	res, err := s.attendanceRepository.List(s.dbManager.GetDB())
	if err != nil {
		return nil, &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  err,
		}
	}

	if len(*res) < 1 {
		return nil, &response.ServiceError{
			Code: http.StatusNotFound,
			Err:  errors.New("data not found!"),
		}
	}

	return res, nil
}
