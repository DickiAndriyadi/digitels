package service

import (
	"digitels/api/model/response"
	"errors"
	"net/http"

	"github.com/jinzhu/gorm"
)

func (s *service) Delete(id, tokenString string) *response.ServiceError {

	createMap := map[string]interface{}{
		"token": tokenString,
	}

	user, err := s.userRepository.Detail(s.dbManager.GetDB(), createMap)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  errors.New("User is not found!"),
			}
		}

		return &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  err,
		}

	}

	if user.Level == "user" {
		return &response.ServiceError{
			Code: http.StatusBadRequest,
			Err:  errors.New("only admin can delete attendance"),
		}
	}

	attendanceMap := map[string]interface{}{
		"id": id,
	}

	_, err = s.attendanceRepository.Detail(s.dbManager.GetDB(), attendanceMap)

	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  errors.New("data not found!"),
			}
		}
		return &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  err,
		}

	}

	deleteMap := map[string]interface{}{
		"id": id,
	}

	errDelete := s.attendanceRepository.Delete(s.dbManager.GetDB(), deleteMap)
	if errDelete != nil {
		return &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  errDelete,
		}
	}

	return nil
}
