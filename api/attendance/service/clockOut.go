package service

import (
	"digitels/api/model"
	"digitels/api/model/payload"
	"digitels/api/model/response"
	"errors"
	"net/http"
	"time"

	"github.com/jinzhu/gorm"
)

func (s *service) ClockOut(id, tokenString string) (*model.Attendance, *response.ServiceError) {

	detailMap := map[string]interface{}{
		"id": id,
	}

	attendance, err := s.attendanceRepository.Detail(s.dbManager.GetDB(), detailMap)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  errors.New("data not found!"),
			}
		}

		return nil, &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  err,
		}
	}

	userMap := map[string]interface{}{
		"token": tokenString,
	}

	user, err := s.userRepository.Detail(s.dbManager.GetDB(), userMap)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  errors.New("User is not found!"),
			}
		}

		return nil, &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  err,
		}

	}

	employeeMap := map[string]interface{}{
		"id": attendance.EmployeeID,
	}

	employee, err := s.employeeRepository.Detail(s.dbManager.GetDB(), employeeMap)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  errors.New("Employee is not found!"),
			}
		}

		return nil, &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  err,
		}

	}

	if user.Level == "user" {
		if user.ID != employee.UserID {
			return nil, &response.ServiceError{
				Code: http.StatusBadRequest,
				Err:  errors.New("cannot clock out other employee"),
			}
		}

	}

	var p payload.ClockOut

	loc, err := time.LoadLocation("Asia/Jakarta")

	p.ClockOut = time.Now().In(loc).Format("2006-01-02 15:04:05")
	p.UpdatedAt = time.Now().In(loc).Format("2006-01-02 15:04:05")

	data, errClockOut := s.attendanceRepository.ClockOut(s.dbManager.GetDB(), &p, id)
	if errClockOut != nil {

		return nil, &response.ServiceError{
			Code: http.StatusInternalServerError,
			Err:  errClockOut,
		}
	}

	return data, nil
}
