package attendance

import (
	"digitels/api/model"
	"digitels/api/model/payload"
	"digitels/api/model/response"
)

type Service interface {
	ClockIn(p *payload.ClockIn, token string) (*model.Attendance, *response.ServiceError)
	ClockOut(id, tokenString string) (*model.Attendance, *response.ServiceError)
	Detail(id string) (*model.Attendance, *response.ServiceError)
	List() (*model.Attendances, *response.ServiceError)
	Delete(id, tokenString string) *response.ServiceError
}
