package controller

import (
	"digitels/api/model/response"
	"net/http"

	"github.com/labstack/echo"
)

func (ctrl *AttendanceController) Detail(c echo.Context) error {

	var (
		result response.ItemsResponse
	)

	id := c.Param("id")

	data, err := ctrl.service.Detail(id)
	if err != nil {
		return err.HttpError()
	}

	result.Data.Items = data

	return c.JSON(http.StatusOK, result)
}
