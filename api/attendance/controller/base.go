package controller

import "digitels/api/attendance"

type AttendanceController struct {
	service attendance.Service
}

func NewController(s attendance.Service) *AttendanceController {
	return &AttendanceController{service: s}
}
