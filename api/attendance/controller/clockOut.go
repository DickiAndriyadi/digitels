package controller

import (
	"digitels/api/model/response"
	"net/http"
	"strings"

	"github.com/labstack/echo"
)

func (ctrl *AttendanceController) ClockOut(c echo.Context) error {

	var (
		result response.ItemsResponse
	)

	id := c.Param("id")
	authorizationHeader := c.Request().Header.Get("Authorization")
	tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

	data, err := ctrl.service.ClockOut(id, tokenString)
	if err != nil {
		return err.HttpError()
	}

	result.Data.Items = data

	return c.JSON(http.StatusOK, result)
}
