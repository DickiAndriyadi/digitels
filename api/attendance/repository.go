package attendance

import (
	"digitels/api/model"
	"digitels/api/model/payload"

	"github.com/jinzhu/gorm"
)

type Repository interface {
	ClockIn(db *gorm.DB, store *model.Attendance) (*model.Attendance, error)
	ValidateCheckIn(db *gorm.DB, id uint64) (int64, error)
	Detail(db *gorm.DB, payload map[string]interface{}) (*model.Attendance, error)
	ClockOut(db *gorm.DB, store *payload.ClockOut, id string) (*model.Attendance, error)
	List(db *gorm.DB) (*model.Attendances, error)
	Delete(db *gorm.DB, deleteMap map[string]interface{}) error
}
