package payload

type ClockOut struct {
	ClockOut  string `json:"clockOut"`
	UpdatedAt string `json:"updatedAt"`
}
