package payload

type UpdateEmployee struct {
	Name      string `json:"name"`
	Division  string `json:"division"`
	Email     string `json:"email"`
	UpdatedAt string `json:"updatedAt"`
}
