package payload

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
)

type ClockIn struct {
	EmployeeID uint64 `json:"employeeId" validate:"required"`
}

func (a ClockIn) Validate() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.EmployeeID, validation.Required),
	)
}
