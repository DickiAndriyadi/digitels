package payload

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
)

type CreateEmployee struct {
	UserID   uint64 `json:"userId" validate:"required"`
	Name     string `json:"name" validate:"required"`
	Email    string `json:"email" validate:"required"`
	Division string `json:"division" validate:"required"`
}

func (a CreateEmployee) Validate() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.UserID, validation.Required),
		validation.Field(&a.Name, validation.Required),
		validation.Field(&a.Email, validation.Required),
		validation.Field(&a.Division, validation.Required),
	)
}
