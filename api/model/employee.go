package model

type (
	Employee struct {
		ID        uint64  `json:"id" gorm:"primary_key"`
		UserID    uint64  `json:"userId" gorm:"column:user_id"`
		Name      string  `json:"name" gorm:"column:name"`
		Email     string  `json:"email" gorm:"column:email"`
		Division  string  `json:"division" gorm:"column:division"`
		CreatedAt string  `json:"createdAt" gorm:"column:created_at"`
		UpdatedAt string  `json:"updatedAt" gorm:"column:updated_at"`
		DeletedAt *string `json:"deletedAt" gorm:"column:deleted_at"`
		User      User    `json:"user,omitempty"`
	}

	Employees []Employee
)

func (Employee) ModelName() string {
	return "Employee"
}

func (Employee) TableName() string {
	return "employee"
}
