package model

type (
	Attendance struct {
		ID         uint64  `json:"id" gorm:"primary_key"`
		EmployeeID uint64  `json:"employeeId" gorm:"column:employee_id"`
		ClockIn    string  `json:"clockIn" gorm:"column:clock_in"`
		ClockOut   string  `json:"clockOut" gorm:"column:clock_out"`
		CreatedAt  string  `json:"createdAt" gorm:"column:created_at"`
		UpdatedAt  string  `json:"updatedAt" gorm:"column:updated_at"`
		DeletedAt  *string `json:"deletedAt" gorm:"column:deleted_at"`
	}

	Attendances []Attendance
)

func (Attendance) ModelName() string {
	return "Attendance"
}

func (Attendance) TableName() string {
	return "attendance"
}
