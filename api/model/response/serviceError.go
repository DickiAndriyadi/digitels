package response

import (
	"net/http"

	"digitels/config/constant"

	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
)

type ServiceError struct {
	Code      int   `json:"errCode"`
	Err       error `json:"message"`
	ModelName string
}

func (serviceErr *ServiceError) HttpError() *echo.HTTPError {
	httpCode := serviceErr.Code

	if gorm.IsRecordNotFoundError(serviceErr.Err) {
		if httpCode == 0 {
			httpCode = http.StatusNotFound
		}
		return echo.NewHTTPError(httpCode, constant.ERROR_MODEL_NOT_FOUND, serviceErr.ModelName)
	}

	if httpCode == 0 {
		httpCode = http.StatusInternalServerError
	}

	return echo.NewHTTPError(httpCode, serviceErr.Err.Error())
}
