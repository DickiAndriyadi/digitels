package model

import jwt "github.com/golang-jwt/jwt/v4"

type JwtCustomClaims struct {
	Username string `json:"username"`
	UserID   uint64 `json:"userId"`
	Level    string `json:"level"`
	jwt.RegisteredClaims
}
