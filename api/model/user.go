package model

type User struct {
	ID        uint64  `json:"id" gorm:"primary_key"`
	Username  string  `json:"username" gorm:"column:username"`
	Password  string  `json:"password" gorm:"column:password"`
	Token     string  `json:"token" gorm:"column:token"`
	Level     string  `json:"level" gorm:"column:level"`
	CreatedAt string  `json:"createdAt" gorm:"column:created_at"`
	UpdatedAt string  `json:"updatedAt" gorm:"column:updated_at"`
	DeletedAt *string `json:"deletedAt" gorm:"column:deleted_at"`
}

func (User) ModelName() string {
	return "User"
}

func (User) TableName() string {
	return "user"
}
