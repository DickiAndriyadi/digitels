package api

import (
	"digitels/api/model/response"
	"net/http"

	"github.com/labstack/echo"
)

func TestApi(c echo.Context) error {
	var (
		result response.DataResponse
	)

	result.Data = "Success Hit API"

	return c.JSON(http.StatusOK, result)
}
