package application

import (
	"errors"
	"net/http"
	"strings"
	"time"

	"github.com/johansetia/jowt"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"digitels/api"
	"digitels/api/model/response"
	"digitels/config/db"

	_userController "digitels/api/user/controller"
	_userRepository "digitels/api/user/repository"
	_userService "digitels/api/user/service"

	_employeeController "digitels/api/employee/controller"
	_employeeRepository "digitels/api/employee/repository"
	_employeeService "digitels/api/employee/service"

	_attendanceController "digitels/api/attendance/controller"
	_attendanceRepository "digitels/api/attendance/repository"
	_attendanceService "digitels/api/attendance/service"
)

type App struct {
	E         *echo.Echo
	DBManager db.DatabaseManager
}

func (app *App) InitializeDatabase(dsn string, dbConnection string) {
	err := app.DBManager.Initialize(dsn, dbConnection)

	if err != nil {
		panic(err)
	}
}

func (app *App) Initialize() {
	app.initializeRoutes()
}

func (app *App) Start(addr string) {
	app.E.Start(addr)
}

func (app *App) initializeRoutes() {
	app.E.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAcceptEncoding},
	}))
	app.E.Use(loggerCfg())
	app.E.HTTPErrorHandler = defaultErr()

	userRepository := _userRepository.NewRepository()
	employeeRepository := _employeeRepository.NewRepository()
	attendanceRepository := _attendanceRepository.NewRepository()

	userService := _userService.NewService(
		app.DBManager,
		userRepository,
	)

	employeeService := _employeeService.NewService(
		app.DBManager,
		employeeRepository,
		userRepository,
	)

	attendanceService := _attendanceService.NewService(
		app.DBManager,
		attendanceRepository,
		employeeRepository,
		userRepository,
	)

	userController := _userController.NewController(userService)
	employeeController := _employeeController.NewController(employeeService)
	attendanceController := _attendanceController.NewController(attendanceService)

	app.E.GET("/", api.TestApi).Name = "Test Api"

	app.E.POST("/login", userController.Login).Name = "Login"
	app.E.POST("/register", userController.Register).Name = "Register"

	l := app.E.Group("/logout")
	l.Use(echo.WrapMiddleware(app.JwtValidation))

	l.POST("", userController.Logout).Name = "Logout"

	t := app.E.Group("/employee")
	t.Use(echo.WrapMiddleware(app.JwtValidation))

	t.POST("", employeeController.Create).Name = "Create Employee"
	t.GET("", employeeController.List).Name = "List Employee"
	t.GET("/:id", employeeController.Detail).Name = "Detail Employee"
	t.DELETE("/:id", employeeController.Delete).Name = "Delete Employee"
	t.PATCH("/:id", employeeController.Update).Name = "Update Employee"

	a := app.E.Group("/attendance")
	a.Use(echo.WrapMiddleware(app.JwtValidation))

	a.POST("", attendanceController.ClockIn).Name = "Clock In"
	a.GET("", attendanceController.List).Name = "List Attendance"
	a.GET("/:id", attendanceController.Detail).Name = "Detail Attendance"
	a.DELETE("/:id", attendanceController.Delete).Name = "Delete Attendance"
	a.PATCH("/:id", attendanceController.ClockOut).Name = "Clock Out"

}

// defaultErr is used to mask an error by echo
func defaultErr() func(err error, c echo.Context) {
	return func(err error, c echo.Context) {
		report, ok := err.(*echo.HTTPError)
		if !ok {
			report = echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}

		cerr := map[string]interface{}{
			"errorCode": report.Code,
			"message":   report.Message,
		}
		c.JSON(report.Code, cerr)
	}
}

// loggerCfg is used to show a log access
func loggerCfg() echo.MiddlewareFunc {
	return middleware.LoggerWithConfig(
		middleware.LoggerConfig{
			Format:           "${time_custom} | ${status}  ⇨ ${method} ${protocol} ${host}${uri} ${latency_human} \n",
			CustomTimeFormat: "15:04:05",
		})
}

var JWToken string = "TestDigitels"

func (app *App) JwtValidation(next http.Handler) http.Handler {
	res := new(response.ServiceError)

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		authorizationHeader := r.Header.Get("Authorization")
		if !strings.Contains(authorizationHeader, "Bearer") {
			res.Code = http.StatusUnauthorized
			res.Err = errors.New("Bearer token is required")
			stop(w, res)
		} else {
			tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)
			jwtMap := map[string]interface{}{
				"token": tokenString,
			}
			user, err := _userRepository.NewRepository().Detail(app.DBManager.GetDB(), jwtMap)

			if user == nil || err != nil {
				res.Code = http.StatusUnauthorized
				res.Err = errors.New("Unauthorized!")
				stop(w, res)
				return
			}

			verify := jowt.Verify(JWToken).SetToken(tokenString)

			if verify.Status() {

				if expired(verify.Payload) {
					res.Code = http.StatusUnauthorized
					res.Err = errors.New("Token expired!")
					stop(w, res)
					return
				} else {
					next.ServeHTTP(w, r)
				}
			} else {
				res.Code = http.StatusUnauthorized
				res.Err = errors.New("Wrong token!")
				stop(w, res)
				return
			}
		}

	})
}

func expired(p jowt.Payload) bool {

	now := time.Now().Unix()
	expired := int64(p["exp"].(float64))
	if now > expired {
		return true
	}
	return false
}

func stop(w http.ResponseWriter, res *response.ServiceError) {
	w.Header().Set("Content-Type", "application/json")

	http.Error(w, res.Err.Error(), res.Code)
	return
}
