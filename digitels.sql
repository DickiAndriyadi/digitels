/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 10.4.24-MariaDB : Database - digitels
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`digitels` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `digitels`;

/*Table structure for table `attendance` */

DROP TABLE IF EXISTS `attendance`;

CREATE TABLE `attendance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `clock_in` datetime DEFAULT NULL,
  `clock_out` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `attendance` */

insert  into `attendance`(`id`,`employee_id`,`clock_in`,`clock_out`,`created_at`,`updated_at`,`deleted_at`) values 
(1,8,'2023-02-25 17:16:24','2023-02-25 20:06:31','2023-02-25 17:16:24','2023-02-25 20:06:31',NULL);

/*Table structure for table `employee` */

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `division` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_user_id` (`user_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

/*Data for the table `employee` */

insert  into `employee`(`id`,`user_id`,`name`,`email`,`division`,`created_at`,`updated_at`,`deleted_at`) values 
(1,1,'dicki','dicki.andriyadi14@gmail.com','technology','2023-02-23 18:19:14','2023-02-23 18:19:14',NULL),
(8,2,'dicki','dicki.andriyadi14@gmail.com','technology','2023-02-25 00:16:35','2023-02-25 00:27:05',NULL),
(9,3,'dicki','dicki.andriyadi14@gmail.com','technology','2023-02-25 00:28:03','2023-02-25 00:48:02',NULL),
(10,4,'dicki','dicki.andriyadi14@gmail.com','technology','2023-02-25 00:46:20','2023-02-25 00:46:20',NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `token` text DEFAULT NULL,
  `level` enum('admin','user') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`password`,`token`,`level`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'dicki','f24195ddcfac2726abc8f9b61d4fffad','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImRpY2tpIiwidXNlcklkIjoxLCJsZXZlbCI6ImFkbWluIiwiaXNzIjoiZGlnaXRlbHMiLCJleHAiOjE2Nzc0MjI2MTcsImlhdCI6MTY3NzMzNjIxN30.PIHLUB19178Jvq7AxhetZ4Fy1hB4s8AcEYsUGYBuWwQ','admin','2023-02-23 15:53:01','2023-02-25 14:43:37',NULL),
(2,'dicki1','f3c1df4e8705795b2fe61e1f8bfc3013','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImRpY2tpMSIsInVzZXJJZCI6MiwibGV2ZWwiOiJ1c2VyIiwiaXNzIjoiZGlnaXRlbHMiLCJleHAiOjE2Nzc0MjI1ODUsImlhdCI6MTY3NzMzNjE4NX0.z1BEVB3g4Uap_Pc9v1A9qxR-mPUq2upjhbOAvynRx3s','user','2023-02-23 15:53:01','2023-02-25 14:43:05',NULL),
(3,'dicki2','bfb1e37717ccd32bf4e3ccda9bded513','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImRpY2tpMiIsInVzZXJJZCI6MywibGV2ZWwiOiJ1c2VyIiwiaXNzIjoiZGlnaXRlbHMiLCJleHAiOjE2Nzc0MTY4NzAsImlhdCI6MTY3NzMzMDQ3MH0.a_8Yaod4SiEuhIUenniKry1p_D_LHMY3MO2lkoxSEC4','user','2023-02-23 15:53:01','2023-02-25 13:07:50',NULL),
(4,'dicki4','572c928cac5f93eb665a151cfb1ea4f1','','user','2023-02-25 00:44:29','2023-02-25 00:44:29',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
