package utils

import (
	"digitels/api/employee"
	"digitels/api/model"
	"digitels/config/db"
	"log"

	"bytes"
	"fmt"
	"html/template"
	"time"

	gomail "gopkg.in/gomail.v2"
)

const CONFIG_SMTP_HOST = "smtp.gmail.com"
const CONFIG_SMTP_PORT = 587
const CONFIG_SENDER_NAME = "Testing SMTP <testing.smtpda@gmail.com>"
const CONFIG_AUTH_EMAIL = "testing.smtpda@gmail.com"
const CONFIG_AUTH_PASSWORD = "aqfukzrbyhjswfzp"

func ReminderClockIn(employee employee.Repository, database db.DatabaseManager) {

	list, _ := employee.List(database.GetDB())

	for _, value := range *list {

		loc, _ := time.LoadLocation("Asia/Jakarta")
		reminderTime := time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 8, 50, 0, 0, loc)
		ticker := time.Tick(time.Minute)

		go func() {
			for {
				select {
				case t := <-ticker:
					if t.Hour() == reminderTime.Hour() && t.Minute() == reminderTime.Minute() {
						fmt.Println("It's time for the reminder!")
						sendEmail(value.Name, value.Email, "Clock In")
					}
				}
			}
		}()

	}
}

func ReminderClockOut(employee employee.Repository, database db.DatabaseManager) {

	list, _ := employee.List(database.GetDB())

	for _, value := range *list {

		loc, _ := time.LoadLocation("Asia/Jakarta")
		reminderTime := time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 17, 50, 0, 0, loc)
		ticker := time.Tick(time.Minute)

		go func() {
			for {
				select {
				case t := <-ticker:
					if t.Hour() == reminderTime.Hour() && t.Minute() == reminderTime.Minute() {
						fmt.Println("It's time for the reminder!")
						sendEmail(value.Name, value.Email, "Clock Out")
					}
				}
			}
		}()

	}
}

func sendEmail(name, email, attend string) {

	mailer := gomail.NewMessage()
	mailer.SetHeader("From", CONFIG_SENDER_NAME)
	mailer.SetHeader("To", email)
	mailer.SetHeader("Subject", "Reminder Attendance")

	emailData := model.Reminder{
		Name:   name,
		Attend: attend,
	}

	var emailBody bytes.Buffer
	t, err := template.ParseFiles("./index.html")
	if err != nil {
		log.Fatal("Error parsing HTML template: ", err)
	}
	if err := t.Execute(&emailBody, emailData); err != nil {
		log.Fatal("Error executing HTML template: ", err)
	}

	mailer.SetBody("text/html", emailBody.String())

	d := gomail.NewDialer(CONFIG_SMTP_HOST, CONFIG_SMTP_PORT, CONFIG_AUTH_EMAIL, CONFIG_AUTH_PASSWORD)

	if err := d.DialAndSend(mailer); err != nil {
		log.Fatal("Error sending email: ", err)
	} else {
		log.Println("Email sent successfully!")
	}
}
