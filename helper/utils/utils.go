package utils

import (
	"crypto/md5"
	"fmt"

	jwt "github.com/golang-jwt/jwt/v4"
)

func CompareCrypt(request, encrypt string) bool {

	data := []byte(request)
	resEncrypt := md5.Sum(data)
	if fmt.Sprintf("%x", resEncrypt) == encrypt {
		return true
	}
	return false
}

func CryptPassword(password string) string {
	data := []byte(password)
	resEncrypt := md5.Sum(data)

	return fmt.Sprintf("%x", resEncrypt)
}

func ParseToken(tokenString string) (jwt.MapClaims, error) {
	token, _ := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte("TestDigitels"), nil
	})

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok && !token.Valid {
		return nil, fmt.Errorf("invalid claims")
	}

	return claims, nil
}
