# Instruction

1. git clone projectnya
2. setting SMTP Email di ```digitels/helper/utils/reminder.go``` dengan milik anda/bisa menggunakan settingan yang saya punya
3. setelah itu import database ```digitels.sql``` yang sudah saya siapkan di repo nya di mysql anda

    - username : dicki || password : dicki || level : admin
    - username : dicki1 || password : dicki1 || level : user
    - dan selanjutnya semua akun dengan level user tinggal diganti di bagian angka belakangnya saja untuk username dan passwordnya

4. import collection ```Postman``` yang sudah saya siapkan untuk coba seluruh requestnya
5. run ```go get``` untuk make sure sudah tersedia librarynya di local machine
6. setelah itu bisa run ```go run main.go``` untuk menjalankan programnya

```Note : untuk waktu clock in dan clock out saya setting di jam 9 pagi dan 6 sore, jadi untuk remindernya saya set di jam 8.50 dan 17.50```
